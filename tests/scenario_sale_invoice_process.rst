=====================================
Sale Account Invoice Process Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install sale_invoice_process::

    >>> config = activate_modules('sale_invoice_process')
