datalife_sale_invoice_process
=====================================

The sale_invoice_process module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_invoice_process/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_invoice_process)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
